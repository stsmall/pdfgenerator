﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace PDFGeneratorTextSharp
{
    class MainClass
    {
        public static void Main(string[] args)
        {

            // This keeps track of the creation time
            DateTime PrintTime = DateTime.Now;

            //Full path to the Unicode Arial file
            string ARIALUNI_TFF = Path.Combine(Environment.CurrentDirectory, "arialuni.ttf");

            //Create a base font object making sure to specify IDENTITY-H
            BaseFont bf = BaseFont.CreateFont(ARIALUNI_TFF, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

            //Create a specific font object
            Font mostFont = new Font(bf, 20, Font.BOLD, BaseColor.BLACK);
            Font titleFont = new Font(bf, 9, Font.UNDERLINE, BaseColor.BLACK);
            Font h1Font = new Font(bf, 11, Font.NORMAL);
            Font bodyFont = new Font(bf, 9, Font.NORMAL, BaseColor.DARK_GRAY);

            //create a document object
            Document doc = new Document(PageSize.A4, 10f, 10f, 120f, 80f);
            //get the current directory
            string path = Environment.CurrentDirectory;
            //create PdfWriter object
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path + "/pdfdoc.pdf", FileMode.Create));
            writer.PageEvent = new ITextEvents();
            //open the document for writing
            doc.Open();

            // Добавление параграфа с текстом
            //Paragraph para = new Paragraph("Hello world. Checking Header Footer", new Font(Font.FontFamily.HELVETICA, 22));
            //para.Alignment = Element.ALIGN_CENTER;
            //doc.Add(para);

            // *************************************************
            // *************** РИСУЕМ ТАБЛИЦУ ******************
            // *************************************************

            PdfPTable table = new PdfPTable(5) { TotalWidth = 520f, LockedWidth = true };
            float[] widths = { 40f, 230f, 40f, 145f, 65f };
            table.SetWidths(widths);
            // Блок названий секций
            // формируем шапку
            var heading = new Phrase("Мы, светлые ребята, неустанно работали на протяжении долгого периода бла-бла-бла, заслужили таких скромных  денежек...", h1Font);
            PdfPCell cell = new PdfPCell(heading) { Colspan = 5, HorizontalAlignment = 1 };
            cell.MinimumHeight = 50f;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);



            // формируем заголовки для столбцов
            var titleNumber = new Phrase("Номер", h1Font);
            PdfPCell numberCell = new PdfPCell(titleNumber) { HorizontalAlignment = 1 };
            numberCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(numberCell);

            var titleSubject = new Phrase("Предмет", h1Font);
            PdfPCell subjectCell = new PdfPCell(titleSubject) { HorizontalAlignment = 1 };
            subjectCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(subjectCell);

            var titleCount = new Phrase("Кол-во", h1Font);
            PdfPCell countCell = new PdfPCell(titleCount) { HorizontalAlignment = 1 };
            countCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(countCell);

            var titleComment = new Phrase("Комментарий", h1Font);
            PdfPCell commentCell = new PdfPCell(titleComment) { HorizontalAlignment = 1 };
            commentCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(commentCell);

            var titleAll = new Phrase("Всего", h1Font);
            PdfPCell allCell = new PdfPCell(titleAll) { HorizontalAlignment = 1 };
            allCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(allCell);

            // Заполняем данными
            for (int i = 0; i < 10; i+=1)
            {

                var numberUserCell = new PdfPCell(new Phrase($"{i}")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE };
                table.AddCell(numberUserCell);
                // Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                var subjectUser = new Phrase("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", h1Font);
                //var subjectUser = new Phrase("Работы по реализации дополнительных фич на сайте.", h1Font);
                var subjectUserCell = new PdfPCell(subjectUser) { VerticalAlignment = Element.ALIGN_MIDDLE };
                table.AddCell(subjectUserCell);
                var countUserCell = new PdfPCell(new Phrase("10")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE };
                table.AddCell(countUserCell);
                var commentUserCell = new PdfPCell(new Phrase("--------------")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE };
                table.AddCell(commentUserCell);
                table.AddCell(countUserCell);

                numberUserCell = new PdfPCell(new Phrase($"{i+1}")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE };
                table.AddCell(numberUserCell);
                subjectUser = new Phrase("Внедрение новых модулей для автоматизации наполнения базы данных..", h1Font);
                subjectUserCell = new PdfPCell(subjectUser) { VerticalAlignment = Element.ALIGN_MIDDLE };
                table.AddCell(subjectUserCell);
                countUserCell = new PdfPCell(new Phrase("25")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE };
                table.AddCell(countUserCell);
                commentUserCell = new PdfPCell(new Phrase("--------------")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE };
                table.AddCell(commentUserCell);
                table.AddCell(countUserCell);

                if (i == 50) 
                {
                    var summary = new Phrase("Общее количество:", h1Font);
                    var summaryCell = new PdfPCell(summary) { Colspan = 4, HorizontalAlignment = Element.ALIGN_RIGHT }; ;
                    table.AddCell(summaryCell);
                    countUserCell = new PdfPCell(new Phrase("35")) { HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE };
                    table.AddCell(countUserCell);

                }

            }

            doc.Add(table);

            // *************************************************
            // ******* СОХРАНЯЕМ И ЗАКРЫВАЕМ ДОКУМЕНТ **********
            // *************************************************
            //close the document
            doc.Close();
            //view the result pdf file

            System.Diagnostics.Process.Start(path + "/pdfdoc.pdf");
        }
    }
}
