﻿using System;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace PDFGeneratorTextSharp
{
    public class ITextEvents : PdfPageEventHelper
    {
        // This is the contentbyte object of the writer
        PdfContentByte cb;

        // we will put the final number of pages in a template
        PdfTemplate headerTemplate, footerTemplate;

        // This keeps track of the creation time
        DateTime PrintTime = DateTime.Now;

        //Full path to the Unicode Arial file
        private static string ARIALUNI_TFF = Path.Combine(Environment.CurrentDirectory, "arialuni.ttf");

        //Create a base font object making sure to specify IDENTITY-H
        private static BaseFont bf = BaseFont.CreateFont(name: ARIALUNI_TFF, encoding: BaseFont.IDENTITY_H, embedded: BaseFont.NOT_EMBEDDED);

        //Create a specific font object
        Font mostFont = new Font(bf, size: 20, style: Font.BOLD, color: BaseColor.BLACK);
        Font titleFont = new Font(bf, 9, Font.UNDERLINE, BaseColor.BLACK);
        Font h1Font = new Font(bf, 11, Font.NORMAL);
        Font bodyFont = new Font(bf, 9, Font.NORMAL, BaseColor.DARK_GRAY);

        #region Fields
        private string _header;
        #endregion

        #region Properties
        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }
        #endregion

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {

            PrintTime = DateTime.Now;
            bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            cb = writer.DirectContent;
            headerTemplate = cb.CreateTemplate(400, 150);
            footerTemplate = cb.CreateTemplate(50, 50);
        }

        public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        {
            base.OnEndPage(writer, document);

            // Добавляем логотип
            var logo = Image.GetInstance("Images/logo.jpg");
            logo.ScaleToFit(150f, 62f);

            var p1Header = new Phrase("\"Акт выполненных работ\"", mostFont);

            //Create PdfTable object
            var pdfTab = new PdfPTable(3);
            float[] width = { 100f, 320f, 100f };
            pdfTab.SetWidths(width);
            pdfTab.TotalWidth = 520f;
            pdfTab.LockedWidth = true;
            //We will have to create separate cells to include image logo and 2 separate strings
            //Row 1
            var pdfCell1 = new PdfPCell(logo);
            var pdfCell2 = new PdfPCell(p1Header);
            var text = "Page " + writer.PageNumber + " of ";
            var textPhrase = new Phrase("Страниц");

            //Add paging to header
            {
                cb.BeginText();
                cb.SetFontAndSize(bf, 12);
                cb.SetTextMatrix(document.PageSize.GetRight(100), document.PageSize.GetTop(45));
                cb.ShowText(text);
                cb.EndText();
                float len = bf.GetWidthPoint(text, 12);
                //Adds "12" in Page 1 of 12
                cb.AddTemplate(headerTemplate, document.PageSize.GetRight(100) + len, document.PageSize.GetTop(45));
            }

            //Add paging to footer
            {
                var leftCol = new Paragraph("Станислав Шияновский\n" + "iOS разработчик", h1Font);
                var rightCol = new Paragraph("Restomania project\n" + "Kuzina Application", h1Font);
                var phone = new Paragraph("Phone +38-0501677352", h1Font);
                var address = new Paragraph("           Company: medved-studio\n" + "   Российская Федерация, Новосибирск", h1Font);
                var fax = new Paragraph("info@medved-studio.com", h1Font);

                leftCol.Alignment = Element.ALIGN_LEFT;
                rightCol.Alignment = Element.ALIGN_RIGHT;
                fax.Alignment = Element.ALIGN_RIGHT;
                phone.Alignment = Element.ALIGN_LEFT;
                address.Alignment = Element.ALIGN_CENTER;

                var footerTbl = new PdfPTable(3) { TotalWidth = 520f, HorizontalAlignment = Element.ALIGN_CENTER, LockedWidth = true };
                float[] footerWidths = { 150f, 220f, 150f };
                footerTbl.SetWidths(footerWidths);
                var footerCell1 = new PdfPCell(leftCol);
                var footerCell2 = new PdfPCell();
                var footerCell3 = new PdfPCell(rightCol);
                var sep = new PdfPCell();
                var footerCell4 = new PdfPCell(phone);
                var footerCell5 = new PdfPCell(address);
                var footerCell6 = new PdfPCell(fax);


                footerCell1.Border = 0;
                footerCell2.Border = 0;
                footerCell3.Border = 0;
                footerCell4.Border = 0;
                footerCell5.Border = 0;
                footerCell6.Border = 0;
                footerCell6.HorizontalAlignment = Element.ALIGN_RIGHT;
                sep.Border = 0;
                sep.FixedHeight = 10f;
                footerCell3.HorizontalAlignment = Element.ALIGN_RIGHT;
                footerCell6.PaddingLeft = 0;
                sep.Colspan = 3;

                footerTbl.AddCell(footerCell1);
                footerTbl.AddCell(footerCell2);
                footerTbl.AddCell(footerCell3);
                footerTbl.AddCell(sep);
                footerTbl.AddCell(footerCell4);
                footerTbl.AddCell(footerCell5);
                footerTbl.AddCell(footerCell6);
                footerTbl.WriteSelectedRows(0, -1, 40, 80, writer.DirectContent);
            }

            var pdfCell3 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), h1Font));
            var pdfCell4 = new PdfPCell();
            var pdfCell5 = new PdfPCell(new Phrase(""));

            //set the alignment of all three cells and set border to 0
            pdfCell1.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfCell3.HorizontalAlignment = Element.ALIGN_RIGHT;
            pdfCell5.HorizontalAlignment = Element.ALIGN_RIGHT;

            //pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;

            //pdfCell1.Colspan = 3;
            //pdfCell2.Colspan = 3;
            pdfCell2.PaddingTop = 9f;
            pdfCell3.PaddingTop = 20f;
            pdfCell5.PaddingTop = 9f;

            pdfCell1.Border = 0;
            pdfCell2.Border = 0;
            pdfCell3.Border = 0;
            pdfCell4.Border = 0;
            pdfCell5.Border = 0;

            //add all three cells into PdfTable
            pdfTab.AddCell(pdfCell1);
            pdfTab.AddCell(pdfCell2);
            pdfTab.AddCell(pdfCell3);
            pdfTab.AddCell(pdfCell4);
            pdfTab.AddCell(pdfCell5);

            pdfTab.TotalWidth = 520f;
            pdfTab.LockedWidth = true;

            //call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
            //first param is start row. -1 indicates there is no end row and all the rows to be included to write
            //Third and fourth param is x and y position to start writing
            pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);
            //set pdfContent value

            //Move the pointer and draw line to separate header section from rest of page
            cb.MoveTo(40, document.PageSize.Height - 100);
            cb.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 100);
            cb.Stroke();

            //Move the pointer and draw line to separate footer section from rest of page
            cb.MoveTo(40, document.PageSize.GetBottom(50));
            cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50));
            cb.Stroke();
        }

        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);

            headerTemplate.BeginText();
            headerTemplate.SetFontAndSize(bf, 12);
            headerTemplate.SetTextMatrix(0, 0);
            headerTemplate.ShowText((writer.PageNumber).ToString());
            headerTemplate.EndText();
        }
    }
}
